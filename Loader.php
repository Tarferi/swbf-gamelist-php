<?php
include_once "includes.php";


class Loader {
	/**
	 * Queries all available servers
	 *
	 * @return multitype:Server
	 */
	public static function getAllServers() {
		try {
			$list = MasterServer::getGameServerList ();
			foreach ( $list as $server ) {
				$server->requestQuery ();
			}
			Server::sendQueries ();
			return $list;
		} catch ( Exception $e ) {
			return null;
		}
	}
	/**
	 * Queries specific server
	 *
	 * @return Server
	 */
	public static function getServer($IP, $Port) {
		try {
			$server = new Server ( $IP, $Port );
			$server->requestQuery ();
			Server::sendQueries ();
			return $server;
		} catch ( Exception $e ) {
			return null;
		}
	}
}


?>