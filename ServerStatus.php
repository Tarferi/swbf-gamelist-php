<?php
include_once "includes.php";
class ServerStatus {
	/**
	 * True if server response was received correctly
	 *
	 * @var bool
	 */
	public $Valid=false;
	/**
	 * CIS or GCW
	 *
	 * @var string
	 */
	public $Era;
	/**
	 * Server name
	 *
	 * @var string
	 */
	public $HostName;
	/**
	 * Game version
	 * Values: 1.0,1.2,...
	 *
	 * @var string
	 */
	public $GameVer;
	/**
	 * Map file name
	 * Values: bes1a,bes2a,...s
	 *
	 * @var string
	 */
	public $MapFileName;
	/**
	 * Map planet name
	 * Values: Naboo,Harbor,...
	 *
	 * @var string
	 */
	public $MapPlanetName;
	/**
	 * Battle name
	 * Values: Theed, Citadel,...
	 *
	 * @var unknown
	 */
	public $MapBattleName;
	/**
	 * Number of players currently present on server
	 *
	 * @var int
	 */
	public $PlayersCount;
	/**
	 * Maximum players on server
	 *
	 * @var int
	 *
	 */
	public $MaximumPlayers;
	/**
	 * Minimum players to start game
	 *
	 * @var int
	 */
	public $MinimumPlayers;
	/**
	 * Whether or not this server is password protected
	 *
	 * @var boolean
	 */
	public $Password;
	/**
	 * Currently unknown field
	 * Values (openplaying,...)
	 *
	 * @var string
	 */
	public $GameMode;
	/**
	 * Whether or not this server believes in team-play
	 *
	 * @var boolean
	 */
	public $TeamPlay;
	/**
	 * Unknown field
	 *
	 * @var int
	 */
	public $FragLimit;
	/**
	 * Unknown field
	 *
	 * @var int
	 */
	public $TimeLimit;
	/**
	 * Whether or not this server has team-damage enabled
	 *
	 * @var bool
	 */
	public $TeamDamage;
	/**
	 * AI units per team
	 *
	 * @var int
	 */
	public $AiPerTeam;
	/**
	 * Whether or not heroes are present on current map
	 *
	 * @var unknown
	 */
	public $Heroes;
	/**
	 * Number of teams
	 *
	 * @var int
	 */
	public $NumberOfTeams;
	
	/**
	 * Whether or not players are teamed automtically
	 *
	 * @var bool
	 */
	public $AutoTeam;
	/**
	 * Current team 1 reinforcements
	 *
	 * @var int
	 */
	public $Team1Reinforcements;
	/**
	 * Current team 2 reinforcements
	 *
	 * @var int
	 */
	public $Team2Reinforcements;
	/**
	 * Server type, like PC or dedicated
	 * Values: integers, meaning unknown
	 *
	 * @var int
	 */
	public $ServerType;
	/**
	 * String representation of AI difficulty
	 * Values: easy, medium, hard,...
	 */
	public $AiDifficulty;
	/**
	 * Show player names option
	 *
	 * @var bool
	 */
	public $ShowPlayerNames;
	/**
	 * Number of seconds of invicibility after spawn
	 *
	 * @var unknown
	 */
	public $InvincibilityTime;
	/**
	 * String representation of team 1 name
	 *
	 * @var string
	 */
	public $Team1Name;
	/**
	 * String representation of team 2 name
	 *
	 * @var unknown
	 */
	public $Team2Name;
	
	/**
	 * Array containing all players (including invalid ones!)
	 *
	 * @var array[Player]
	 */
	public $Players;
	
	/**
	 * Array containing all teams (including invalid ones!)
	 *
	 * @var array[Team]
	 */
	public $Teams;
	private $str;
	private function resolveMapName() {
		$code = $this->MapFileName;
		$last = substr ( $code, strlen ( $code ) - 1, 1 );
		$era = "Galactic Civil War";
		switch ($last) {
			case "r" :
			case "c" :
				$era = "Clone Wars";
		}
		$this->Era = $era;
		$map = substr ( $code, 0, strlen ( $code ) - 1 );
		$m = "Unknown";
		$p = "Unknown";
		switch ($map) {
			case "bes2" :
				$p = "Bespin";
				$m = "Cloud City";
				break;
			case "bes1" :
				$p = "Bespin";
				$m = "Platforms";
				break;
			case "end1" :
				$p = "Endor";
				$m = "Bunker";
				break;
			case "geo1" :
				$p = "Geonosis";
				$m = "Spire";
				break;
			case "hot1" :
				$p = "Hoth";
				$m = "Echo Base";
				break;
			case "kam1" :
				$p = "Kamino";
				$m = "Topica City";
				break;
			case "kam2" :
				$p = "Kamino";
				$m = "Nuricoca City";
				break;
			case "kas1" :
				$p = "Kashyyyk";
				$m = "Docks";
				break;
			case "kas2" :
				$p = "Kashyyyk";
				$m = "Islands";
				break;
			case "nab1" :
				$p = "Naboo";
				$m = "Theed";
				break;
			case "nab2" :
				$p = "Naboo";
				$m = "Plains";
				break;
			case "rhn1" :
				$p = "Rhen Var";
				$m = "Harbour";
				break;
			case "rhn2" :
				$p = "Rhen Var";
				$m = "Citadel";
				break;
			case "tat1" :
				$p = "Tatooine";
				$m = "Dune Sea";
				break;
			case "tat2" :
				$p = "Tatooine";
				$m = "Mos Eisley";
				break;
			case "tat3" :
				$p = "Tatooine";
				$m = "Jabba's Place";
				break;
			case "yav1" :
				$p = "Yavin 4";
				$m = "Temple";
				break;
			case "yav2" :
				$p = "Yavin 4";
				$m = "Arena";
				break;
			case "corus2" :
				$p = "Coruscant";
				$m = "Streets";
				break;
		}
		$this->MapPlanetName = $p;
		$this->MapBattleName = $m;
	}
	
	/**
	 * I shall construct myself
	 */
	private function __construct($str) {
		$this->str = $str;
		$teamsValid = true;
		$playersValid = true;
		if ($this->get () == 0) {
			$diff = $this->get ();
			if ($diff == 0x9f) {
				$this->get ();
				$this->get ();
				$this->get ();
				while ( $this->hasRemaining () ) {
					$key = $this->getString ();
					if (strlen ( $key ) == 0) {
						$this->get ();
						$this->get ();
						$players = array ();
						$playerkeys = array ();
						while ( $this->hasRemaining () ) {
							$key = $this->getString ();
							if (strlen ( $key ) == 0) {
								break;
							} else {
								$playerkeys [] = $key;
							}
						}
						$pc = count ( $playerkeys );
						$id = 0;
						while ( $this->hasRemaining () ) {
							$key = $this->getString ();
							if (strlen ( $key ) == 0) {
								break;
							} else {
								$players [$id] = array ();
								$players [$id] [$playerkeys [0]] = $key;
								for($i = 1; $i < $pc; $i ++) {
									$players [$id] [$playerkeys [$i]] = $this->getString ();
								}
							}
							$id ++;
						}
						$pls = array ();
						foreach ( $players as $i => $player ) {
							$pl = new Player ();
							$pl->Name = $this->getOrDefault ( $player, "player_", null );
							$pl->Score = $this->getOrDefault ( $player, "score_", null );
							$pl->Team = $this->getOrDefault ( $player, "team_", null );
							$pl->CapturedCPS = $this->getOrDefault ( $player, "cps_", null );
							$pl->Deaths = $this->getOrDefault ( $player, "deaths_", null );
							$pl->Ping = $this->getOrDefault ( $player, "ping_", null );
							$pl->Validate ();
							$pls [] = $pl;
							if (! $pl->Valid) {
								$playersValid = false;
							}
						}
						$this->get ();
						$teams = array ();
						$teamkeys = array ();
						while ( $this->hasRemaining () ) {
							$key = $this->getString ();
							if (strlen ( $key ) == 0) {
								break;
							} else {
								$teamkeys [] = $key;
							}
						}
						$pc = count ( $teamkeys );
						$id = 0;
						while ( $this->hasRemaining () ) {
							$key = $this->getString ();
							if (strlen ( $key ) == 0) {
								break;
							} else {
								$teams [$id] = array ();
								$k = $this->getOrDefault ( $teamkeys, 0, null );
								$teams [$id] [$k] = $key;
								for($i = 1; $i < $pc; $i ++) {
									$k = $this->getOrDefault ( $teamkeys, $i, null );
									$teams [$id] [$k] = $this->getString ();
								}
							}
							$id ++;
						}
						$tms = array ();
						foreach ( $teams as $i => $team ) {
							$score = $this->getOrDefault ( $team, "score_t", null );
							$name = $this->getOrDefault ( $team, "team_t", null );
							$tm = new Team ( $name, $score );
							$tm->validate ();
							if ($tm->Valid) {
								$tms [] = $tm;
							} else {
								$teamsValid = false;
							}
						}
						foreach ( $pls as $player ) {
							if ($player->Team != null) {
								$player->Team = $this->getOrDefault ( $tms, $player->Team - 1, null );
							}
						}
						$this->Teams = $tms;
						$this->Players = $pls;
						$tn = $this->getOrDefault ( $tms, 0, null );
						if ($tn != null) {
							$this->Team1Name = $tn->TeamName;
						}
						$tn = $this->getOrDefault ( $tms, 1, null );
						if ($tn != null) {
							$this->Team2Name = $tn->TeamName;
						}
						
						break;
					} else {
						switch (strtolower ( $key )) {
							default :
								$this->getString ();
								break;
							case "hostname" :
								$this->HostName = $this->getString ();
								break;
							case "gamever" :
								$this->GameVer = $this->getString ();
								break;
							case "mapname" :
								$this->MapFileName = $this->getString ();
								$this->resolveMapName ();
								break;
							case "gametype" :
								$this->GameType = $this->getString ();
								break;
							case "numplayers" :
								$this->PlayersCount = 1 * $this->getString ();
								break;
							case "password" :
								$this->Password = $this->getString () == "1";
								break;
							case "maxplayers" :
								$this->MaximumPlayers = $this->getString ();
								break;
							case "gamemode" :
								$this->GameMode = $this->getString ();
								break;
							case "teamplay" :
								$this->TeamPlay = $this->getString () == "1";
								break;
							case "fraglimit" :
								$this->FragLimit = $this->getString () * 1;
								break;
							case "timelimit" :
								$this->TimeLimit = $this->getString () * 1;
								break;
							case "teamdamage" :
								$this->TeamDamage = $this->getString () == "1";
								break;
							case "heroes" :
								$this->Heroes = $this->getString () == "1";
								break;
							case "autoteam" :
								$this->AutoTeam = $this->getString () == "1";
								break;
							case "numai" :
								$this->AiPerTeam = $this->getString ();
								break;
							case "team1reinforcements" :
								$this->Team1Reinforcements = $this->getString () * 1;
								break;
							case "team2reinforcements" :
								$this->Team2Reinforcements = $this->getString () * 1;
								break;
							case "servertype" :
								$this->ServerType = $this->getString () * 1;
								break;
							case "minplayers" :
								$this->MinimumPlayers = $this->getString () * 1;
								break;
							case "aidifficulty" :
								$this->AiDifficulty = $this->getAiDifficulty ( $this->getString () * 1 );
								break;
							case "showplayernames" :
								$this->ShowPlayerNames = $this->getString () == "1";
								break;
							case "invincibilitytime" :
								$this->InvincibilityTime = $this->getString () * 1;
								break;
							case "numteams" :
								$this->NumberOfTeams = $this->getString () * 1;
								break;
						}
					}
				}
			}
		}
		$this->Valid = ! $this->hasRemaining () && $playersValid && $teamsValid;
		$this->str=null;
	}
	private function getOrDefault($player, $key, $default) {
		if (isset ( $player [$key] )) {
			return $player [$key];
		} else {
			return $default;
		}
	}
	private function getAiDifficulty($i) {
		switch ($i) {
			case 1 :
				return "Easy";
			case 2 :
				return "Medium";
			case 3 :
				return "Hard";
			default :
				return "Unknown";
		}
	}
	
	/**
	 * Parses server status from query response string
	 *
	 * @param string $str        	
	 * @return ServerStatus
	 */
	public static function getStatus($str) {
		$st = new ServerStatus ( $str );
		return $st;
	}
	
	/**
	 * True if response is not empty
	 *
	 * @return boolean
	 */
	private function hasRemaining() {
		return strlen ( $this->str ) > 0;
	}
	
	/**
	 * Returns string from response string
	 *
	 * @return string
	 */
	private function getString() {
		$str = "";
		while ( true ) {
			$c = $this->get ();
			if ($c == 0) {
				break;
			} else {
				$str .= chr ( $c );
			}
		}
		return $str;
	}
	
	/**
	 * Reads single byte from response string
	 *
	 * @return number
	 */
	private function get() {
		if (! $this->hasRemaining ()) {
			return 0;
		} else {
			$c = ord ( substr ( $this->str, 0, 1 ) );
			$this->str = substr ( $this->str, 1 );
			return $c;
		}
	}
}

?>