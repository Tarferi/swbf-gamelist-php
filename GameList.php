<?php



/**
 * Cached results are valid for 10 seconds and are stored in folder server_cache
 */
$cache=new Cache(10,"server_cache");

/**
 * Get all server data
*/
$servers = $cache->getAllServers();

/**
 * Get specific server data
*/
$server=$cache->getServer("162.248.88.173",3660);


class Cache {
	
	/**
	 * Number of seconds to keep last result cached
	 *
	 * @var int
	 */
	private $cache_delay;
	/**
	 * Cache storage folder
	 *
	 * @var string
	 */
	private $folder;
	
	/**
	 * File path to file containing last result time
	 *
	 * @var string
	 */
	private $ltime;
	
	/**
	 * Last result file path
	 *
	 * @var string
	 */
	private $rfile;
	
	/**
	 * Current time stamp
	 *
	 * @var int
	 */
	private $ctime;
	
	/**
	 * Constructs cache object
	 *
	 * @param int $cache_delay        	
	 * @param string $folder        	
	 */
	public function __construct($cache_delay, $folder = "cache") {
		$this->folder = $folder;
		$this->cache_delay = $cache_delay;
		$this->createFolderIfNotExists ();
		$this->ctime = time ();
		$this->rfile = $folder . "/last_result";
		$this->ltime = $folder . "/last_time";
	}
	
	/**
	 * Returns queried servers or cached servers if they had been queried recently
	 *
	 * @return multitype:Server |Ambigous <Server, multitype:Server >
	 */
	public function getAllServers() {
		$lt = $this->ltime;
		$rf = $this->rfile;
		if ($this->validCacheDataAvailable ( $lt, $rf )) {
			$this->updateLastTime ( $lt );
			$servers = Loader::getAllServers ();
			$this->updateLastResult ( $servers, $rf );
			return $servers;
		} else {
			return $this->getLastResult ( $rf );
		}
	}
	
	/**
	 * Returns true if valid cached data is available
	 * 
	 * @param unknown $lt        	
	 * @param unknown $rf        	
	 * @return boolean
	 */
	public function validCacheDataAvailable($lt, $rf) {
		return $this->ctime - $this->getLastResultTime ( $lt, $rf ) > $this->cache_delay;
	}
	
	/**
	 * Returns queried server or cached server if it had been queried recently
	 *
	 * @param string $IP        	
	 * @param string $Port        	
	 * @return Server
	 */
	public function getServer($IP, $Port) {
		$lt = $this->resolveLtimeForServer ( $IP, $Port );
		$rf = $this->resolveRfileForServer ( $IP, $Port );
		if ($this->validCacheDataAvailable ( $lt, $rf )) {
			$this->updateLastTime ( $lt );
			$server = Loader::getServer ( $IP, $Port );
			$this->updateLastResult ( $server, $rf );
			return $server;
		} else {
			return $this->getLastResult ( $rf );
		}
	}
	/**
	 * Resolve server-specific cache objet
	 *
	 * @param string $IP        	
	 * @param int $Port        	
	 * @return string
	 */
	private function resolveLtimeForServer($IP, $Port) {
		return $this->ltime . "_" . md5 ( $IP . ":" . $Port );
	}
	/**
	 * Resolve server-speficif cache object
	 *
	 * @param string $IP        	
	 * @param int $Port        	
	 * @return string
	 */
	private function resolveRfileForServer($IP, $Port) {
		return $this->rfile . "_" . md5 ( $IP . ":" . $Port );
	}
	/**
	 * Updates last result (Saves to file)
	 *
	 * @param Server|Array[Server] $data        	
	 * @param string $rfile        	
	 */
	private function updateLastResult($data, $rfile) {
		file_put_contents ( $rfile, json_encode ( $data ) );
	}
	/**
	 * Updates last result time
	 *
	 * @param string $ltime        	
	 */
	private function updateLastTime($ltime) {
		file_put_contents ( $ltime, $this->ctime );
	}
	/**
	 * Returns last cached result object
	 *
	 * @param string $rf        	
	 * @return Server|array[Server]
	 */
	private function getLastResult($rf) {
		return json_decode ( file_get_contents ( $rf ) );
	}
	/**
	 * Returns last result time
	 *
	 * @param string $lt        	
	 * @param string $rf        	
	 * @return number
	 */
	private function getLastResultTime($lt, $rf) {
		if (file_exists ( $lt ) && file_exists ( $rf )) {
			if (is_dir ( $this->folder ) && is_file ( $rf )) {
				return file_get_contents ( $lt ) * 1;
			}
		}
		return 0;
	}
	/**
	 * Creates cache folder if it does not exist yet
	 */
	private function createFolderIfNotExists() {
		if (file_exists ( $this->folder )) {
			if (is_dir ( $this->folder )) {
				return;
			}
		}
		mkdir ( $this->folder );
	}
}





class Loader {
	/**
	 * Queries all available servers
	 *
	 * @return multitype:Server
	 */
	public static function getAllServers() {
		try {
			$list = MasterServer::getGameServerList ();
			foreach ( $list as $server ) {
				$server->requestQuery ();
			}
			Server::sendQueries ();
			return $list;
		} catch ( Exception $e ) {
			return null;
		}
	}
	/**
	 * Queries specific server
	 *
	 * @return Server
	 */
	public static function getServer($IP, $Port) {
		try {
			$server = new Server ( $IP, $Port );
			$server->requestQuery ();
			Server::sendQueries ();
			return $server;
		} catch ( Exception $e ) {
			return null;
		}
	}
}




class MasterServer {
	/**
	 * Master server timeout in seconds
	 * @var unknown
	 */
	const TimeOut = 1;
	
	/**
	 * String representing byte sequence to be sent to master server as query request
	 *
	 * @var String
	 */
	const masterSeq = "00a00001030000000073776266726f6e7470630073776266726f6e747063006223273833522b4a005c686f73746e616d655c67616d656d6f64655c6d61706e616d655c67616d657665725c6e756d706c61796572735c6d6178706c61796572735c67616d65747970655c73657373696f6e5c7072657673657373696f6e5c737762726567696f6e5c736572766572747970655c70617373776f72640000000000";
	
	/**
	 * IP of master server
	 *
	 * @var string
	 */
	const IP = "5.230.233.61";
	/**
	 * Port of master server
	 *
	 * @var int
	 */
	const Port = 28910;
	
	/**
	 * Returns Server array containing only ip and port, all other fields empty
	 * Request must be made on each server separately in Server object
	 *
	 * @throws QueryFailedException
	 * @return multitype:Server
	 */
	public static final function getGameServerList() {
		$sock = socket_create ( AF_INET, SOCK_STREAM, SOL_TCP );
		if (! $sock) {
			throw new QueryFailedException ();
		}
		if (! socket_connect ( $sock, MasterServer::IP, MasterServer::Port )) {
			throw new QueryFailedException ();
		}
		socket_set_option ( $sock, SOL_SOCKET, SO_RCVTIMEO, array (
				"sec" => MasterServer::TimeOut,
				"usec" => 0 
		) );
		socket_write ( $sock, MasterServer::hexStreamToString ( MasterServer::masterSeq ) );
		socket_close($sock);
		$resp = MasterServer::readServerResponse ( $sock );
		$arr = MasterServer::gameServerListDecrypt ( $resp );
		$darr = MasterServer::ipArrayToServerArray ( $arr );
		return $darr;
	}
	/**
	 *
	 * @param
	 *        	multitype:string
	 * @return multitype:Server
	 */
	private static final function ipArrayToServerArray($arr) {
		$a = array ();
		foreach ( $arr as $data ) {
			$r = explode ( ":", $data );
			$ip = $r [0];
			$port = $r [1] * 1;
			$a [] = new Server ( $ip, $port );
		}
		return $a;
	}
	
	/**
	 * Painful decryptor, needs to be rewritten
	 *
	 * @param string $encryptedData        	
	 * @return multitype:string
	 */
	private static final function gameServerListDecrypt($encryptedData) {
		return array (
				"5.9.132.3:3658",
				"84.62.13.247:33533",
				"216.52.143.105:3661",
				"216.52.143.105:3663",
				"216.52.143.105:3659",
				"216.52.148.181:3659",
				"216.52.143.105:3658",
				"216.52.148.181:3658",
				"216.52.143.105:3660",
				"5.9.104.177:3658",
				"216.52.143.105:3662",
				"5.9.104.177:3659",
				"162.248.88.173:3658",
				"162.248.88.173:3659",
				"162.248.88.173:3660" 
		);
	}
	
	/**
	 * Reads and returns string reprezentation of master server response
	 *
	 * @param socket $sock        	
	 * @return string
	 */
	private static final function readServerResponse($sock) {
		$str = "";
		while ( true ) {
			$data = @socket_read ( $sock, 1 );
			if (! $data) {
				break;
			}
			$str .= $data;
		}
		return $str;
	}
	
	/**
	 * Private function to transform hex stream to string
	 *
	 * @return string:
	 */
	public static final function hexStreamToString($data) {
		$ar = "";
		$len = strlen ( $data );
		for($i = 0; $i < $len; $i += 2) {
			$ar .= chr ( hexdec ( substr ( $data, $i, 2 ) ) );
		}
		return $ar;
	}
}

/**
 * Thrown when socket cannot be created
 *
 * @author Donny Brook
 *        
 */
class QueryFailedException extends Exception {
}



class Player {
	public $Name;
	public $Score;
	/**
	 * Experimental
	 *
	 * @var bool
	 */
	public $Valid=false;
	public $Team;
	public $CapturedCPS;
	public $Deaths;
	public $Ping;
	
	/**
	 * Validates the player
	 * Sets ValidPlayer to True if player is valid
	 */
	public function Validate() {
		$this->Valid = $this->Name != null && $this->Score != null && $this->Team != null && $this->CapturedCPS != null && $this->Deaths != null && $this->Ping != null;
	}
}



class Server {
	/**
	 * Whether or not the server has been queries successfuly.
	 * This field is false only if the server did not respond.
	 * 
	 * @var bool
	 */
	public $Valid = false;
	/**
	 * Query socket timeout
	 *
	 * @var int
	 */
	const TimeOut = 1;
	/**
	 * Local port for binding (random; > 1000)
	 *
	 * @var unknown
	 */
	const localQueryPort = 48151;
	/**
	 * Array of server to query, indexed by ip:port
	 *
	 * @var array[Server]
	 */
	private static $queries = array ();
	private static $queryMessage = "fefd009f120300ffffff";
	
	/**
	 * Server IP
	 *
	 * @var string
	 */
	public $IP;
	/**
	 * Server port
	 *
	 * @var int
	 */
	public $Port;
	
	/**
	 * Status of the server
	 *
	 * @var Status
	 */
	public $Status;
	public function __construct($ip, $port) {
		$this->IP = $ip;
		$this->Port = $port;
	}
	
	/**
	 * Adds query request
	 */
	public function requestQuery() {
		Server::$queries [$this->IP . ":" . $this->Port] = $this;
	}
	private function receiveQueryResponse($queryString) {
		$this->Status = ServerStatus::getStatus ( $queryString );
		$this->Valid = true;
	}
	
	/**
	 * Sends all query requests and parse all responses
	 *
	 * @throws QueryFailedException When query fails
	 */
	public static final function sendQueries() {
		$sock = socket_create ( AF_INET, SOCK_DGRAM, SOL_UDP );
		if (! $sock) {
			throw new QueryFailedException ();
		}
		if (! socket_bind ( $sock, "0.0.0.0", Server::localQueryPort )) {
			throw new QueryFailedException ();
		}
		$qm = MasterServer::hexStreamToString ( Server::$queryMessage );
		$qs = Server::$queries;
		foreach ( $qs as $query ) {
			socket_sendto ( $sock, $qm, strlen ( $qm ), 0, $query->IP, $query->Port );
		}
		socket_set_option ( $sock, SOL_SOCKET, SO_RCVTIMEO, array (
				"sec" => Server::TimeOut,
				"usec" => 0 
		) );
		Server::parseConnections ( $qs, $sock );
		socket_close ( $sock );
	}
	
	/**
	 * Parses server connection
	 *
	 * @param array[Server] $qs
	 *        	callback servers
	 * @param socket $conn
	 *        	connection
	 */
	private static final function parseConnections($qs, $sock) {
		while ( Server::QueriesAvailable ( $qs ) ) {
			$size = @socket_recvfrom ( $sock, $data, 65535, 0, $ip, $port );
			if ($size) {
				$key = $ip . ":" . $port;
				if (isset ( $qs [$key] )) {
					$server = $qs [$key];
					$qs [$key] = null;
					$server->receiveQueryResponse ( $data );
				} else {
					echo "Received packet from unqueried server: " . $key . "\n";
				}
			} else {
				break;
			}
		}
	}
	private static final function QueriesAvailable($qs) {
		foreach ( $qs as $q ) {
			if ($q != null) {
				return true;
			}
		}
		return false;
	}
}



class ServerStatus {
	/**
	 * True if server response was received correctly
	 *
	 * @var bool
	 */
	public $Valid=false;
	/**
	 * CIS or GCW
	 *
	 * @var string
	 */
	public $Era;
	/**
	 * Server name
	 *
	 * @var string
	 */
	public $HostName;
	/**
	 * Game version
	 * Values: 1.0,1.2,...
	 *
	 * @var string
	 */
	public $GameVer;
	/**
	 * Map file name
	 * Values: bes1a,bes2a,...s
	 *
	 * @var string
	 */
	public $MapFileName;
	/**
	 * Map planet name
	 * Values: Naboo,Harbor,...
	 *
	 * @var string
	 */
	public $MapPlanetName;
	/**
	 * Battle name
	 * Values: Theed, Citadel,...
	 *
	 * @var unknown
	 */
	public $MapBattleName;
	/**
	 * Number of players currently present on server
	 *
	 * @var int
	 */
	public $PlayersCount;
	/**
	 * Maximum players on server
	 *
	 * @var int
	 *
	 */
	public $MaximumPlayers;
	/**
	 * Minimum players to start game
	 *
	 * @var int
	 */
	public $MinimumPlayers;
	/**
	 * Whether or not this server is password protected
	 *
	 * @var boolean
	 */
	public $Password;
	/**
	 * Currently unknown field
	 * Values (openplaying,...)
	 *
	 * @var string
	 */
	public $GameMode;
	/**
	 * Whether or not this server believes in team-play
	 *
	 * @var boolean
	 */
	public $TeamPlay;
	/**
	 * Unknown field
	 *
	 * @var int
	 */
	public $FragLimit;
	/**
	 * Unknown field
	 *
	 * @var int
	 */
	public $TimeLimit;
	/**
	 * Whether or not this server has team-damage enabled
	 *
	 * @var bool
	 */
	public $TeamDamage;
	/**
	 * AI units per team
	 *
	 * @var int
	 */
	public $AiPerTeam;
	/**
	 * Whether or not heroes are present on current map
	 *
	 * @var unknown
	 */
	public $Heroes;
	/**
	 * Number of teams
	 *
	 * @var int
	 */
	public $NumberOfTeams;
	
	/**
	 * Whether or not players are teamed automtically
	 *
	 * @var bool
	 */
	public $AutoTeam;
	/**
	 * Current team 1 reinforcements
	 *
	 * @var int
	 */
	public $Team1Reinforcements;
	/**
	 * Current team 2 reinforcements
	 *
	 * @var int
	 */
	public $Team2Reinforcements;
	/**
	 * Server type, like PC or dedicated
	 * Values: integers, meaning unknown
	 *
	 * @var int
	 */
	public $ServerType;
	/**
	 * String representation of AI difficulty
	 * Values: easy, medium, hard,...
	 */
	public $AiDifficulty;
	/**
	 * Show player names option
	 *
	 * @var bool
	 */
	public $ShowPlayerNames;
	/**
	 * Number of seconds of invicibility after spawn
	 *
	 * @var unknown
	 */
	public $InvincibilityTime;
	/**
	 * String representation of team 1 name
	 *
	 * @var string
	 */
	public $Team1Name;
	/**
	 * String representation of team 2 name
	 *
	 * @var unknown
	 */
	public $Team2Name;
	
	/**
	 * Array containing all players (including invalid ones!)
	 *
	 * @var array[Player]
	 */
	public $Players;
	
	/**
	 * Array containing all teams (including invalid ones!)
	 *
	 * @var array[Team]
	 */
	public $Teams;
	private $str;
	private function resolveMapName() {
		$code = $this->MapFileName;
		$last = substr ( $code, strlen ( $code ) - 1, 1 );
		$era = "Galactic Civil War";
		switch ($last) {
			case "r" :
			case "c" :
				$era = "Clone Wars";
		}
		$this->Era = $era;
		$map = substr ( $code, 0, strlen ( $code ) - 1 );
		$m = "Unknown";
		$p = "Unknown";
		switch ($map) {
			case "bes2" :
				$p = "Bespin";
				$m = "Cloud City";
				break;
			case "bes1" :
				$p = "Bespin";
				$m = "Platforms";
				break;
			case "end1" :
				$p = "Endor";
				$m = "Bunker";
				break;
			case "geo1" :
				$p = "Geonosis";
				$m = "Spire";
				break;
			case "hot1" :
				$p = "Hoth";
				$m = "Echo Base";
				break;
			case "kam1" :
				$p = "Kamino";
				$m = "Topica City";
				break;
			case "kam2" :
				$p = "Kamino";
				$m = "Nuricoca City";
				break;
			case "kas1" :
				$p = "Kashyyyk";
				$m = "Docks";
				break;
			case "kas2" :
				$p = "Kashyyyk";
				$m = "Islands";
				break;
			case "nab1" :
				$p = "Naboo";
				$m = "Theed";
				break;
			case "nab2" :
				$p = "Naboo";
				$m = "Plains";
				break;
			case "rhn1" :
				$p = "Rhen Var";
				$m = "Harbour";
				break;
			case "rhn2" :
				$p = "Rhen Var";
				$m = "Citadel";
				break;
			case "tat1" :
				$p = "Tatooine";
				$m = "Dune Sea";
				break;
			case "tat2" :
				$p = "Tatooine";
				$m = "Mos Eisley";
				break;
			case "tat3" :
				$p = "Tatooine";
				$m = "Jabba's Place";
				break;
			case "yav1" :
				$p = "Yavin 4";
				$m = "Temple";
				break;
			case "yav2" :
				$p = "Yavin 4";
				$m = "Arena";
				break;
			case "corus2" :
				$p = "Coruscant";
				$m = "Streets";
				break;
		}
		$this->MapPlanetName = $p;
		$this->MapBattleName = $m;
	}
	
	/**
	 * I shall construct myself
	 */
	private function __construct($str) {
		$this->str = $str;
		$teamsValid = true;
		$playersValid = true;
		if ($this->get () == 0) {
			$diff = $this->get ();
			if ($diff == 0x9f) {
				$this->get ();
				$this->get ();
				$this->get ();
				while ( $this->hasRemaining () ) {
					$key = $this->getString ();
					if (strlen ( $key ) == 0) {
						$this->get ();
						$this->get ();
						$players = array ();
						$playerkeys = array ();
						while ( $this->hasRemaining () ) {
							$key = $this->getString ();
							if (strlen ( $key ) == 0) {
								break;
							} else {
								$playerkeys [] = $key;
							}
						}
						$pc = count ( $playerkeys );
						$id = 0;
						while ( $this->hasRemaining () ) {
							$key = $this->getString ();
							if (strlen ( $key ) == 0) {
								break;
							} else {
								$players [$id] = array ();
								$players [$id] [$playerkeys [0]] = $key;
								for($i = 1; $i < $pc; $i ++) {
									$players [$id] [$playerkeys [$i]] = $this->getString ();
								}
							}
							$id ++;
						}
						$pls = array ();
						foreach ( $players as $i => $player ) {
							$pl = new Player ();
							$pl->Name = $this->getOrDefault ( $player, "player_", null );
							$pl->Score = $this->getOrDefault ( $player, "score_", null );
							$pl->Team = $this->getOrDefault ( $player, "team_", null );
							$pl->CapturedCPS = $this->getOrDefault ( $player, "cps_", null );
							$pl->Deaths = $this->getOrDefault ( $player, "deaths_", null );
							$pl->Ping = $this->getOrDefault ( $player, "ping_", null );
							$pl->Validate ();
							$pls [] = $pl;
							if (! $pl->Valid) {
								$playersValid = false;
							}
						}
						$this->get ();
						$teams = array ();
						$teamkeys = array ();
						while ( $this->hasRemaining () ) {
							$key = $this->getString ();
							if (strlen ( $key ) == 0) {
								break;
							} else {
								$teamkeys [] = $key;
							}
						}
						$pc = count ( $teamkeys );
						$id = 0;
						while ( $this->hasRemaining () ) {
							$key = $this->getString ();
							if (strlen ( $key ) == 0) {
								break;
							} else {
								$teams [$id] = array ();
								$k = $this->getOrDefault ( $teamkeys, 0, null );
								$teams [$id] [$k] = $key;
								for($i = 1; $i < $pc; $i ++) {
									$k = $this->getOrDefault ( $teamkeys, $i, null );
									$teams [$id] [$k] = $this->getString ();
								}
							}
							$id ++;
						}
						$tms = array ();
						foreach ( $teams as $i => $team ) {
							$score = $this->getOrDefault ( $team, "score_t", null );
							$name = $this->getOrDefault ( $team, "team_t", null );
							$tm = new Team ( $name, $score );
							$tm->validate ();
							if ($tm->Valid) {
								$tms [] = $tm;
							} else {
								$teamsValid = false;
							}
						}
						foreach ( $pls as $player ) {
							if ($player->Team != null) {
								$player->Team = $this->getOrDefault ( $tms, $player->Team - 1, null );
							}
						}
						$this->Teams = $tms;
						$this->Players = $pls;
						$tn = $this->getOrDefault ( $tms, 0, null );
						if ($tn != null) {
							$this->Team1Name = $tn->TeamName;
						}
						$tn = $this->getOrDefault ( $tms, 1, null );
						if ($tn != null) {
							$this->Team2Name = $tn->TeamName;
						}
						
						break;
					} else {
						switch (strtolower ( $key )) {
							default :
								$this->getString ();
								break;
							case "hostname" :
								$this->HostName = $this->getString ();
								break;
							case "gamever" :
								$this->GameVer = $this->getString ();
								break;
							case "mapname" :
								$this->MapFileName = $this->getString ();
								$this->resolveMapName ();
								break;
							case "gametype" :
								$this->GameType = $this->getString ();
								break;
							case "numplayers" :
								$this->PlayersCount = 1 * $this->getString ();
								break;
							case "password" :
								$this->Password = $this->getString () == "1";
								break;
							case "maxplayers" :
								$this->MaximumPlayers = $this->getString ();
								break;
							case "gamemode" :
								$this->GameMode = $this->getString ();
								break;
							case "teamplay" :
								$this->TeamPlay = $this->getString () == "1";
								break;
							case "fraglimit" :
								$this->FragLimit = $this->getString () * 1;
								break;
							case "timelimit" :
								$this->TimeLimit = $this->getString () * 1;
								break;
							case "teamdamage" :
								$this->TeamDamage = $this->getString () == "1";
								break;
							case "heroes" :
								$this->Heroes = $this->getString () == "1";
								break;
							case "autoteam" :
								$this->AutoTeam = $this->getString () == "1";
								break;
							case "numai" :
								$this->AiPerTeam = $this->getString ();
								break;
							case "team1reinforcements" :
								$this->Team1Reinforcements = $this->getString () * 1;
								break;
							case "team2reinforcements" :
								$this->Team2Reinforcements = $this->getString () * 1;
								break;
							case "servertype" :
								$this->ServerType = $this->getString () * 1;
								break;
							case "minplayers" :
								$this->MinimumPlayers = $this->getString () * 1;
								break;
							case "aidifficulty" :
								$this->AiDifficulty = $this->getAiDifficulty ( $this->getString () * 1 );
								break;
							case "showplayernames" :
								$this->ShowPlayerNames = $this->getString () == "1";
								break;
							case "invincibilitytime" :
								$this->InvincibilityTime = $this->getString () * 1;
								break;
							case "numteams" :
								$this->NumberOfTeams = $this->getString () * 1;
								break;
						}
					}
				}
			}
		}
		$this->Valid = ! $this->hasRemaining () && $playersValid && $teamsValid;
		$this->str=null;
	}
	private function getOrDefault($player, $key, $default) {
		if (isset ( $player [$key] )) {
			return $player [$key];
		} else {
			return $default;
		}
	}
	private function getAiDifficulty($i) {
		switch ($i) {
			case 1 :
				return "Easy";
			case 2 :
				return "Medium";
			case 3 :
				return "Hard";
			default :
				return "Unknown";
		}
	}
	
	/**
	 * Parses server status from query response string
	 *
	 * @param string $str        	
	 * @return ServerStatus
	 */
	public static function getStatus($str) {
		$st = new ServerStatus ( $str );
		return $st;
	}
	
	/**
	 * True if response is not empty
	 *
	 * @return boolean
	 */
	private function hasRemaining() {
		return strlen ( $this->str ) > 0;
	}
	
	/**
	 * Returns string from response string
	 *
	 * @return string
	 */
	private function getString() {
		$str = "";
		while ( true ) {
			$c = $this->get ();
			if ($c == 0) {
				break;
			} else {
				$str .= chr ( $c );
			}
		}
		return $str;
	}
	
	/**
	 * Reads single byte from response string
	 *
	 * @return number
	 */
	private function get() {
		if (! $this->hasRemaining ()) {
			return 0;
		} else {
			$c = ord ( substr ( $this->str, 0, 1 ) );
			$this->str = substr ( $this->str, 1 );
			return $c;
		}
	}
}



class Team {
	/**
	 *
	 * @var string
	 */
	public $TeamName;
	/**
	 *
	 * @var int
	 */
	public $TeamScore;
	/**
	 * Whether or not this object is valid
	 * 
	 * @var unknown
	 */
	public $Valid=false;
	public function __construct($TeamName, $TeamScore) {
		$this->TeamName = $TeamName;
		$this->TeamScore = $TeamScore * 1;
	}
	public function validate() {
		$this->Valid = $this->TeamName != null && $this->TeamScore != null;
	}
}










?>