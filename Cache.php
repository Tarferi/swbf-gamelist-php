<?php
include_once "includes.php";

/**
 * Cached results are valid for 10 seconds and are stored in folder server_cache
 */
$cache=new Cache(10,"server_cache");

/**
 * Get all server data
*/
//$servers = $cache->getAllServers();

/**
 * Get specific server data
*/
//$server=$cache->getServer("162.248.88.173",3660);


class Cache {
	
	/**
	 * Number of seconds to keep last result cached
	 *
	 * @var int
	 */
	private $cache_delay;
	/**
	 * Cache storage folder
	 *
	 * @var string
	 */
	private $folder;
	
	/**
	 * File path to file containing last result time
	 *
	 * @var string
	 */
	private $ltime;
	
	/**
	 * Last result file path
	 *
	 * @var string
	 */
	private $rfile;
	
	/**
	 * Current time stamp
	 *
	 * @var int
	 */
	private $ctime;
	
	/**
	 * Constructs cache object
	 *
	 * @param int $cache_delay        	
	 * @param string $folder        	
	 */
	public function __construct($cache_delay, $folder = "cache") {
		$this->folder = $folder;
		$this->cache_delay = $cache_delay;
		$this->createFolderIfNotExists ();
		$this->ctime = time ();
		$this->rfile = $folder . "/last_result";
		$this->ltime = $folder . "/last_time";
	}
	
	/**
	 * Returns queried servers or cached servers if they had been queried recently
	 *
	 * @return multitype:Server |Ambigous <Server, multitype:Server >
	 */
	public function getAllServers() {
		$lt = $this->ltime;
		$rf = $this->rfile;
		if ($this->validCacheDataAvailable ( $lt, $rf )) {
			$this->updateLastTime ( $lt );
			$servers = Loader::getAllServers ();
			$this->updateLastResult ( $servers, $rf );
			return $servers;
		} else {
			return $this->getLastResult ( $rf );
		}
	}
	
	/**
	 * Returns true if valid cached data is available
	 * 
	 * @param unknown $lt        	
	 * @param unknown $rf        	
	 * @return boolean
	 */
	public function validCacheDataAvailable($lt, $rf) {
		return $this->ctime - $this->getLastResultTime ( $lt, $rf ) > $this->cache_delay;
	}
	
	/**
	 * Returns queried server or cached server if it had been queried recently
	 *
	 * @param string $IP        	
	 * @param string $Port        	
	 * @return Server
	 */
	public function getServer($IP, $Port) {
		$lt = $this->resolveLtimeForServer ( $IP, $Port );
		$rf = $this->resolveRfileForServer ( $IP, $Port );
		if ($this->validCacheDataAvailable ( $lt, $rf )) {
			$this->updateLastTime ( $lt );
			$server = Loader::getServer ( $IP, $Port );
			$this->updateLastResult ( $server, $rf );
			return $server;
		} else {
			return $this->getLastResult ( $rf );
		}
	}
	/**
	 * Resolve server-specific cache objet
	 *
	 * @param string $IP        	
	 * @param int $Port        	
	 * @return string
	 */
	private function resolveLtimeForServer($IP, $Port) {
		return $this->ltime . "_" . md5 ( $IP . ":" . $Port );
	}
	/**
	 * Resolve server-speficif cache object
	 *
	 * @param string $IP        	
	 * @param int $Port        	
	 * @return string
	 */
	private function resolveRfileForServer($IP, $Port) {
		return $this->rfile . "_" . md5 ( $IP . ":" . $Port );
	}
	/**
	 * Updates last result (Saves to file)
	 *
	 * @param Server|Array[Server] $data        	
	 * @param string $rfile        	
	 */
	private function updateLastResult($data, $rfile) {
		file_put_contents ( $rfile, json_encode ( $data ) );
	}
	/**
	 * Updates last result time
	 *
	 * @param string $ltime        	
	 */
	private function updateLastTime($ltime) {
		file_put_contents ( $ltime, $this->ctime );
	}
	/**
	 * Returns last cached result object
	 *
	 * @param string $rf        	
	 * @return Server|array[Server]
	 */
	private function getLastResult($rf) {
		return json_decode ( file_get_contents ( $rf ) );
	}
	/**
	 * Returns last result time
	 *
	 * @param string $lt        	
	 * @param string $rf        	
	 * @return number
	 */
	private function getLastResultTime($lt, $rf) {
		if (file_exists ( $lt ) && file_exists ( $rf )) {
			if (is_dir ( $this->folder ) && is_file ( $rf )) {
				return file_get_contents ( $lt ) * 1;
			}
		}
		return 0;
	}
	/**
	 * Creates cache folder if it does not exist yet
	 */
	private function createFolderIfNotExists() {
		if (file_exists ( $this->folder )) {
			if (is_dir ( $this->folder )) {
				return;
			}
		}
		mkdir ( $this->folder );
	}
}

?>