<?php
include_once "includes.php";
class Server {
	/**
	 * Whether or not the server has been queries successfuly.
	 * This field is false only if the server did not respond.
	 * 
	 * @var bool
	 */
	public $Valid = false;
	/**
	 * Query socket timeout
	 *
	 * @var int
	 */
	const TimeOut = 1;
	/**
	 * Local port for binding (random; > 1000)
	 *
	 * @var unknown
	 */
	const localQueryPort = 48151;
	/**
	 * Array of server to query, indexed by ip:port
	 *
	 * @var array[Server]
	 */
	private static $queries = array ();
	private static $queryMessage = "fefd009f120300ffffff";
	
	/**
	 * Server IP
	 *
	 * @var string
	 */
	public $IP;
	/**
	 * Server port
	 *
	 * @var int
	 */
	public $Port;
	
	/**
	 * Status of the server
	 *
	 * @var Status
	 */
	public $Status;
	public function __construct($ip, $port) {
		$this->IP = $ip;
		$this->Port = $port;
	}
	
	/**
	 * Adds query request
	 */
	public function requestQuery() {
		Server::$queries [$this->IP . ":" . $this->Port] = $this;
	}
	private function receiveQueryResponse($queryString) {
		$this->Status = ServerStatus::getStatus ( $queryString );
		$this->Valid = true;
	}
	
	/**
	 * Sends all query requests and parse all responses
	 *
	 * @throws QueryFailedException When query fails
	 */
	public static final function sendQueries() {
		$sock = socket_create ( AF_INET, SOCK_DGRAM, SOL_UDP );
		if (! $sock) {
			throw new QueryFailedException ();
		}
		if (! socket_bind ( $sock, "0.0.0.0", Server::localQueryPort )) {
			throw new QueryFailedException ();
		}
		$qm = MasterServer::hexStreamToString ( Server::$queryMessage );
		$qs = Server::$queries;
		foreach ( $qs as $query ) {
			socket_sendto ( $sock, $qm, strlen ( $qm ), 0, $query->IP, $query->Port );
		}
		socket_set_option ( $sock, SOL_SOCKET, SO_RCVTIMEO, array (
				"sec" => Server::TimeOut,
				"usec" => 0 
		) );
		Server::parseConnections ( $qs, $sock );
		socket_close ( $sock );
	}
	
	/**
	 * Parses server connection
	 *
	 * @param array[Server] $qs
	 *        	callback servers
	 * @param socket $conn
	 *        	connection
	 */
	private static final function parseConnections($qs, $sock) {
		while ( Server::QueriesAvailable ( $qs ) ) {
			$size = @socket_recvfrom ( $sock, $data, 65535, 0, $ip, $port );
			if ($size) {
				$key = $ip . ":" . $port;
				if (isset ( $qs [$key] )) {
					$server = $qs [$key];
					$qs [$key] = null;
					$server->receiveQueryResponse ( $data );
				} else {
					echo "Received packet from unqueried server: " . $key . "\n";
				}
			} else {
				break;
			}
		}
	}
	private static final function QueriesAvailable($qs) {
		foreach ( $qs as $q ) {
			if ($q != null) {
				return true;
			}
		}
		return false;
	}
}

?>