<?php
include_once "includes.php";
class Decoder {
	private static function array_copy($src, $isrc, $dst, $idst, $fdst) {
		$o=$isrc;
		for($i=$idst;$i<$fdst;$i++) {
			$dst[$o]=$src[$i];
			$o++;
		}
		return $dst;
	}
	private static function enctypex_decoder($key, $validate, $data, $datalen, $enctypex_data) {
		$encxkeyb = array ();
		$encxkey = null;
		if ($enctypex_data->argValue == null) {
			$encxkey = $encxkeyb;
		} else {
			$encxkey = $enctypex_data->argValue->encxkey;
		}
		if ($enctypex_data->argValue == null || $enctypex_data->argValue->start != null) {
			$tempRef_encxkey = new RefObject ( $encxkey );
			$data->argValue = Decoder::enctypex_init ( $tempRef_encxkey, $key, $validate, $data, $datalen, $enctypex_data );
			$encxkey = $tempRef_encxkey->argValue;
			if ($data->argValue == null) {
				echo "Wrong1\n";
				return null;
			}
		}
		if ($enctypex_data->argValue == null) {
			$tempRef_encxkey2 = new RefObject ( $encxkey );
			Decoder::enctypex_func6 ( $tempRef_encxkey2, $data, $datalen->argValue );
			$encxkey = $tempRef_encxkey2->argValue;
			return $data->argValue;
		} else if ($enctypex_data->argValue->start != null) {
			$temp1 = array ();
			$temp1 = Decoder::array_copy ( $data->argValue, $enctypex_data->argValue->offset, $temp1, 0, $datalen->argValue - $enctypex_data->argValue->offset );
			$tempRef_encxkey3 = new RefObject ( $encxkey );
			$tempRef_temp1 = new RefObject ( $temp1 );
			$temp3 = Decoder::enctypex_func6 ( $tempRef_encxkey3, $tempRef_temp1, $datalen->argValue - $enctypex_data->argValue->offset );
			$encxkey = $tempRef_encxkey3->argValue;
			$temp1 = $tempRef_temp1->argValue;
			$data->argValue = Decoder::array_copy ( $temp1, 0, $data->argValue, $enctypex_data->argValue->offset, $datalen->argValue - $enctypex_data->argValue->offset );
			$enctypex_data->argValue->offset += $temp3;
			$temp2 = array ();
			$temp2 = Decoder::array_copy ( $data->argValue, $enctypex_data->argValue->start, $temp2, 0, $datalen->argValue - $enctypex_data->argValue->start );
			return $temp2;
		}
		echo "Wrong2\n";
		return null;
	}
	private static function enctypex_init($encxkey, $key, $validate, $data, $datalen, $enctypex_data) {
		$a = 0;
		$b = 0;
		$encxvalidate = array ();
		if ($datalen->argValue < 1) {
			echo "Wrong3\n";
			return (null);
		}
		$a = ((($data->argValue [0] & 0xff) ^ 0xec)) + 2;
		if ($datalen->argValue < $a) {
			echo "Wrong4\n";
			return (null);
		}
		$b = ($data->argValue [$a - 1] & 0xff) ^ 0xea;
		if ($datalen->argValue < ($a + $b)) {
			echo "Wrong5\n";
			return (null);
		}
		$encxvalidate = Decoder::array_copy ( $validate->argValue, 0, $encxvalidate, 0, 8 );
		$temp1 = array ();
		$temp1 = Decoder::array_copy ( $data->argValue, $a, $temp1, 0, $datalen->argValue - $a );
		$tempRef_encxvalidate = new RefObject ( $encxvalidate );
		$tempRef_temp1 = new RefObject ( $temp1 );
		Decoder::enctypex_funcx ( $encxkey, $key, $tempRef_encxvalidate, $tempRef_temp1, $b );
		$encxvalidate = $tempRef_encxvalidate->argValue;
		$temp1 = $tempRef_temp1->argValue;
		$data->argValue = Decoder::array_copy ( $temp1, 0, $data->argValue, $a, $datalen->argValue - $a );
		$a += $b;
		$enctypex_data->argValue = new enctypex_data_t ();
		if ($enctypex_data->argValue == null) {
			$temp2 = array ();
			$temp2 = Decoder::array_copy ( $data->argValue, $a, $temp2, 0, $datalen->argValue - $a );
			$data->argValue = $temp2;
			$datalen->argValue -= $a;
		} else {
			$enctypex_data->argValue->offset = $a;
			$enctypex_data->argValue->start = $a;
		}
		return $data->argValue;
	}
	private static function enctypex_func6($encxkey, $data, $len) {
		$i = 0;
		// TODO: Check with java source
		$data->argValue = array_values ( $data->argValue );
		for($i = 0; $i <= $len - 1; $i ++) {
			$data->argValue [$i] = Decoder::enctypex_func7 ( $encxkey, $data->argValue [$i] );
		}
		return $len;
	}
	private static function enctypex_func7($encxkey, $d) {
		$a = 0;
		$b = 0;
		$c = 0;
		$encxkey->argValue=array_values($encxkey->argValue);
		$a = $encxkey->argValue [256] & 0xff;
		$b = $encxkey->argValue [257] & 0xff;
		$c = $encxkey->argValue [$a & 0xff] & 0xff;
		$encxkey->argValue [256] = (($a + 1) % 256) & 0xff;
		$encxkey->argValue [257] = (($b + $c) % 256) & 0xff;
		$a = $encxkey->argValue [260] & 0xff;
		$b = $encxkey->argValue [257] & 0xff;
		$b = $encxkey->argValue [$b] & 0xff;
		$c = $encxkey->argValue [$a] & 0xff;
		$encxkey->argValue [$a] = $b & 0xff;
		$a = $encxkey->argValue [259] & 0xff;
		$b = $encxkey->argValue [257] & 0xff;
		$a = $encxkey->argValue [$a] & 0xff;
		$encxkey->argValue [$b & 0xff] = $a & 0xff;
		$a = $encxkey->argValue [256] & 0xff;
		$b = $encxkey->argValue [259] & 0xff;
		$a = $encxkey->argValue [$a] & 0xff;
		$encxkey->argValue [$b] = $a & 0xff;
		$a = $encxkey->argValue [256] & 0xff;
		$encxkey->argValue [$a] = $c & 0xff;
		$b = $encxkey->argValue [258] & 0xff;
		$a = $encxkey->argValue [$c] & 0xff;
		$c = $encxkey->argValue [259] & 0xff;
		$b = ($b + $a) % 256;
		$encxkey->argValue [258] = $b & 0xff;
		$a = $b;
		$c = $encxkey->argValue [$c] & 0xff;
		$b = $encxkey->argValue [257] & 0xff;
		$b = $encxkey->argValue [$b] & 0xff;
		$a = $encxkey->argValue [$a] & 0xff;
		$c = ($c + $b) % 256;
		$b = $encxkey->argValue [260] & 0xff;
		$b = $encxkey->argValue [$b] & 0xff;
		$c = ($c + $b) % 256;
		$b = $encxkey->argValue [$c] & 0xff;
		$c = $encxkey->argValue [256] & 0xff;
		$c = $encxkey->argValue [$c] & 0xff;
		$a = ($a + $c) % 256;
		$c = $encxkey->argValue [$b] & 0xff;
		$b = $encxkey->argValue [$a] & 0xff;
		$encxkey->argValue [260] = $d & 0xff;
		$c = ($c ^ $b ^ ($d & 0xff)) % 256;
		$encxkey->argValue [259] = $c & 0xff;
		return $c & 0xff;
	}
	private static function enctypex_funcx($encxkey, $key, $encxvalidate, $data, $datalen) {
		$i = 0;
		$keylen = 0;
		$keylen = count ( $key->argValue );
		// TODO: Check with java source
		$data->argValue = array_values ( $data->argValue );
		for($i = 0; $i <= $datalen - 1; $i ++) {
			$encxvalidate->argValue [($key->argValue [$i % $keylen] * $i) & 7] = 0xff & ($encxvalidate->argValue [($key->argValue [$i % $keylen] * $i) & 7] ^ ($encxvalidate->argValue [$i & 7] & 0xff) ^ ($data->argValue [$i]) & 0xff);
		}
		Decoder::enctypex_func4 ( $encxkey, $encxvalidate, new RefObject ( 8 ) );
	}
	private static function enctypex_func4($encxkey, $id, $idlen) {
		$i = 0;
		$n1 = 0;
		$n2 = 0;
		$t1 = 0;
		$t2 = 0;
		if ($idlen->argValue < 1) {
			return;
		}
		for($i = 0; $i <= 255; $i ++) {
			$encxkey->argValue [$i] = $i & 0xff;
		}
		for($i = 255; $i >= 0; $i += - 1) {
			$tempRef_i = new RefObject ( $i );
			$tempRef_n1 = new RefObject ( $n1 );
			$tempRef_n2 = new RefObject ( $n2 );
			$t1 = 0xff & Decoder::enctypex_func5 ( $encxkey, $tempRef_i, $id, $idlen, $tempRef_n1, $tempRef_n2 );
			$i = $tempRef_i->argValue;
			$n1 = $tempRef_n1->argValue;
			$n2 = $tempRef_n2->argValue;
			$t2 = $encxkey->argValue [$i];
			$encxkey->argValue [$i] = $encxkey->argValue [($t1 & 0xff)];
			$encxkey->argValue [($t1 & 0xff)] = $t2 & 0xff;
		}
		$encxkey->argValue [256] = $encxkey->argValue [1];
		$encxkey->argValue [257] = $encxkey->argValue [3];
		$encxkey->argValue [258] = $encxkey->argValue [5];
		$encxkey->argValue [259] = $encxkey->argValue [7];
		$encxkey->argValue [260] = $encxkey->argValue [($n1 & 0xff)];
	}
	private static function enctypex_func5($encxkey, $cnt, $id, $idlen, $n1, $n2) {
		$i = 0;
		$tmp = 0;
		$mask = 1;
		if ($cnt->argValue == 0) {
			return 0;
		}
		if ($cnt->argValue > 1) {
			do {
				$mask = ($mask << 1) + 1;
			} while ( $mask < $cnt->argValue );
		}
		$i = 0;
		do {
			$n1->argValue = (($encxkey->argValue [($n1->argValue & 0xff)] & 0xff) + ($id->argValue [$n2->argValue]) & 0xff);
			$n2->argValue += 1;
			if ($n2->argValue >= $idlen->argValue) {
				$n2->argValue = 0;
				$n1->argValue += $idlen->argValue;
			}
			$tmp = $n1->argValue & $mask;
			$i += 1;
			if ($i > 11) {
				$tmp = $tmp % $cnt->argValue;
			}
		} while ( $tmp > $cnt->argValue );
		return ($tmp);
	}
	public static function getBytes($str) {
		$s = array ();
		for($i = 0, $o = strlen ( $str ); $i < $o; $i ++) {
			$s [] = ord ( substr ( $str, $i, 1 ) );
		}
		return $s;
	}
	public static function getServers($key, $validate, $data) {
		$k = new RefObject ( Decoder::getBytes ( $key ) );
		$v = new RefObject ( Decoder::getBytes ( $validate ) );
		$d = new RefObject ( $data );
		$l = new RefObject ( count ( $data ) );
		$p = new RefObject ( null );
		$t = Decoder::enctypex_decoder ( $k, $v, $d, $l, $p );
		//var_dump ( $t );
		return Decoder::getServerList ( $t );
	}
	private static function toString($data) {
		$s="";
		foreach($data as $b) {
			$s.=chr($b);
		}
		return $s;
	}
	private static function getServerList($Data) {
		try {
			var_dump(Decoder::toString($Data));
			die();
			$b = new ByteBuffer ( $Data );
			Decoder::getIP ( $b );
			Decoder::getPort ( $b );
			$b->get ();
			$b->get ();
			while ( true ) {
				Decoder::getString ( $b );
				$c = $b->get ();
				if ($c != 0) {
					break;
				}
				$c = $b->get ();
				if ($c == 0x15) {
					$b->setPosition ( $b->getPosition () - 1 );
					break;
				}
			}
			$s = array ();
			while ( true ) {
				$c = $b->get ();
				if ($c == 0) {
					break;
				} else {
					$s [] = new Server ( Decoder::getIP ( $b ), Decoder::getPort ( $b ) );
				}
			}
			return $s;
		} catch ( exception $e ) {
			var_dump ( $e );
			return array ();
		}
	}
	private static function getIP($b) {
		$data = array ();
		$data [0] = $b->get ();
		$data [1] = $b->get ();
		$data [2] = $b->get ();
		$data [3] = $b->get ();
		return ($data [0] & 0xff) . "." . ($data [1] & 0xff) . "." . ($data [2] & 0xff) . "." . ($data [3] & 0xff);
	}
	private static function getPort($b) {
		$data = array ();
		$data [0] = $b->get ();
		$data [1] = $b->get ();
		return ((($data [0] & 0xff) << 8) | (($data [1] & 0xff)));
	}
	private static function getString($b) {
		$sb = "";
		while ( true ) {
			$c = $b->get ();
			if ($c == 0) {
				break;
			} else {
				$sb .= chr ( $c );
			}
		}
		return $sb;
	}
}
function getData() {
	$str = file_get_contents ( "sock" );
	return Decoder::getBytes ( $str );
}
$s = Decoder::getServers ( "y3Hd2d", "b#'83R+J", getData () );
//var_dump ( $s );
class RefObject {
	public $argValue;
	public function __construct($refArg) {
		$this->argValue = $refArg;
	}
}
class enctypex_data_t {
	public $encxkey = array ();
	public $offset;
	public $start;
}
class ByteBuffer {
	private $Data;
	private $Pos = 0;
	private $Size;
	public function get() {
		$el = $this->Data [$this->Pos];
		$this->Pos ++;
		return $el;
	}
	public function getPosition() {
		return $this->Pos;
	}
	public function setPosition($pos) {
		$this->Pos = $pos;
	}
	public function __construct($data) {
		// TODO: Check semantics
		$data = array_values ( $data );
		$this->Data = $data;
		$this->Size = count ( $this->Data );
	}
}
?>