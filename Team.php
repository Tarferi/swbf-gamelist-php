<?php
include_once "includes.php";
class Team {
	/**
	 *
	 * @var string
	 */
	public $TeamName;
	/**
	 *
	 * @var int
	 */
	public $TeamScore;
	/**
	 * Whether or not this object is valid
	 * 
	 * @var unknown
	 */
	public $Valid=false;
	public function __construct($TeamName, $TeamScore) {
		$this->TeamName = $TeamName;
		$this->TeamScore = $TeamScore * 1;
	}
	public function validate() {
		$this->Valid = $this->TeamName != null && $this->TeamScore != null;
	}
}

?>