<?php
include_once "includes.php";
class Player {
	public $Name;
	public $Score;
	/**
	 * Experimental
	 *
	 * @var bool
	 */
	public $Valid=false;
	public $Team;
	public $CapturedCPS;
	public $Deaths;
	public $Ping;
	
	/**
	 * Validates the player
	 * Sets ValidPlayer to True if player is valid
	 */
	public function Validate() {
		$this->Valid = $this->Name != null && $this->Score != null && $this->Team != null && $this->CapturedCPS != null && $this->Deaths != null && $this->Ping != null;
	}
}

?>