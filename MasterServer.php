<?php
include_once "includes.php";
class MasterServer {
	/**
	 * Master server timeout in seconds
	 * @var unknown
	 */
	const TimeOut = 1;
	
	/**
	 * String representing byte sequence to be sent to master server as query request
	 *
	 * @var String
	 */
	const masterSeq = "00a00001030000000073776266726f6e7470630073776266726f6e747063006223273833522b4a005c686f73746e616d655c67616d656d6f64655c6d61706e616d655c67616d657665725c6e756d706c61796572735c6d6178706c61796572735c67616d65747970655c73657373696f6e5c7072657673657373696f6e5c737762726567696f6e5c736572766572747970655c70617373776f72640000000000";
	
	/**
	 * IP of master server
	 *
	 * @var string
	 */
	const IP = "5.230.233.61";
	/**
	 * Port of master server
	 *
	 * @var int
	 */
	const Port = 28910;
	
	/**
	 * Returns Server array containing only ip and port, all other fields empty
	 * Request must be made on each server separately in Server object
	 *
	 * @throws QueryFailedException
	 * @return multitype:Server
	 */
	public static final function getGameServerList() {
		$sock = socket_create ( AF_INET, SOCK_STREAM, SOL_TCP );
		if (! $sock) {
			throw new QueryFailedException ();
		}
		if (! socket_connect ( $sock, MasterServer::IP, MasterServer::Port )) {
			throw new QueryFailedException ();
		}
		socket_set_option ( $sock, SOL_SOCKET, SO_RCVTIMEO, array (
				"sec" => MasterServer::TimeOut,
				"usec" => 0 
		) );
		socket_write ( $sock, MasterServer::hexStreamToString ( MasterServer::masterSeq ) );
		socket_close($sock);
		$resp = MasterServer::readServerResponse ( $sock );
		$arr = MasterServer::gameServerListDecrypt ( $resp );
		$darr = MasterServer::ipArrayToServerArray ( $arr );
		return $darr;
	}
	/**
	 *
	 * @param
	 *        	multitype:string
	 * @return multitype:Server
	 */
	private static final function ipArrayToServerArray($arr) {
		$a = array ();
		foreach ( $arr as $data ) {
			$r = explode ( ":", $data );
			$ip = $r [0];
			$port = $r [1] * 1;
			$a [] = new Server ( $ip, $port );
		}
		return $a;
	}
	
	/**
	 * Painful decryptor, needs to be rewritten
	 *
	 * @param string $encryptedData        	
	 * @return multitype:string
	 */
	private static final function gameServerListDecrypt($encryptedData) {
		return array (
				"5.9.132.3:3658",
				"84.62.13.247:33533",
				"216.52.143.105:3661",
				"216.52.143.105:3663",
				"216.52.143.105:3659",
				"216.52.148.181:3659",
				"216.52.143.105:3658",
				"216.52.148.181:3658",
				"216.52.143.105:3660",
				"5.9.104.177:3658",
				"216.52.143.105:3662",
				"5.9.104.177:3659",
				"162.248.88.173:3658",
				"162.248.88.173:3659",
				"162.248.88.173:3660"
		);
	}
	
	/**
	 * Reads and returns string reprezentation of master server response
	 *
	 * @param socket $sock        	
	 * @return string
	 */
	private static final function readServerResponse($sock) {
		$str = "";
		while ( true ) {
			$data = @socket_read ( $sock, 1 );
			if (! $data) {
				break;
			}
			$str .= $data;
		}
		return $str;
	}
	
	/**
	 * Private function to transform hex stream to string
	 *
	 * @return string:
	 */
	public static final function hexStreamToString($data) {
		$ar = "";
		$len = strlen ( $data );
		for($i = 0; $i < $len; $i += 2) {
			$ar .= chr ( hexdec ( substr ( $data, $i, 2 ) ) );
		}
		return $ar;
	}
}

/**
 * Thrown when socket cannot be created
 *
 * @author Donny Brook
 *        
 */
class QueryFailedException extends Exception {
}

?>