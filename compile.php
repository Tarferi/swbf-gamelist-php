<?php
$lst = scandir ( "." );
$resfile = "GameList.php";
$thisfile = "compile.php";
$res = "";
foreach ( $lst as $f ) {
	if (strlen ( $f ) > 4) {
		if (strtolower ( substr ( $f, strlen ( $f ) - 4, 4 ) ) == ".php" && strtolower ( $f ) != strtolower ( $resfile ) && strtolower ( $f ) != strtolower ( $thisfile )) {
			$str = file_get_contents ( $f );
			$str = str_replace ( array (
					"?>",
					"<?php" 
			), "", $str );
			$str = preg_replace ( "/include_once \"[\w\. ]*\";/im", "", $str );
			$res .= $str;
		}
	}
}
$res = "<?php\r\n" . $res . "\r\n?>";
file_put_contents ( $resfile, $res );

?>